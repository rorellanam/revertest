var args = arguments[0] || {};

/**
 * @property {Object} newsInfo Contains a list with the news data
 * @private
*/
var newsInfo = {};

/**
 * @property {Object} listItem Holds list item to be loaded in list view
 * @private
*/
var listItem = {};

/**
 * @method init
 * @private
 * Initializes controller
 * @return {void}
 */
function init() {
    var formattedDate;
    var convertDateTimeToUTC;
    var localDateTime;
    
    newsInfo = args.newsInfo || {};
    
    listItem = {
        template: 'newsTemplate',
        title: {},
        pubDate: {},
        link: {}
    }

    convertDateTimeToUTC = moment.utc(newsInfo.pubDate);
    localDateTime = moment(convertDateTimeToUTC).local().format("YYYY-MM-DD HH:mm:ss");
    
    formattedDate = moment(localDateTime).fromNow();
    
    listItem.title.text = newsInfo.title;
    listItem.pubDate.text = formattedDate;
    listItem.link.text = newsInfo.link;
    
}

/**
 * @method getListItem
 * @public
 * Gets list item data
 * @return {Object}
 */
$.getListItem = function() {
    return listItem;
};


init();