var navigation = require('navigation');
var webservices = require('webservices');

/**
 * @method init
 * @private
 * Initializes controller
 * @return {void}
 */
function init() {
    navigation.openIndexWindow($.window);
    $.activityIndicator.show();
    fetchNews();
}

/**
 * @method fetchNews
 * @private
 * Gets news from an endpoint given
 * @return {void}
 */
function fetchNews() {
    webservices.fetchNews({
        successCallback: handleFetchNewsSuccess,
        failCallback: handleFetchNewsFailcalback
    });
}

/**
 * @method handleFetchNewsSuccess
 * @private
 * Handles success request
 * @param {Object} _responseData Holds response data
 * @return {void}
 */
function handleFetchNewsSuccess(_responseData) {
    var responseData = _responseData || {};
    loadNewsData(responseData);
    $.activityIndicator.hide();
}

/**
 * @method handleFetchNewsFailcalback
 * @private
 * Handles fail or error on the http request
 * @param {Object} _responseData Holds response data
 * @return {void}
 */
function handleFetchNewsFailcalback(_responseData) {
    var status = _responseData && _responseData.status;
    
    switch(status) {
        case 'connectionError':
            alert('Internet Connection Error \nYou need to have connection');
            break;
        default:
            alert('Something went wrong, please try later');
    }
    
    _.defer(function() {
        $.activityIndicator.hide();
    });
}

/**
 * @method loadNewsData
 * @private
 * Loads ui with the response data
 * @param {Object} _responseData Holds response data
 * @return {void}
 */
function loadNewsData(_responseData) {
    var newsData = _responseData || {};
    var listItems = [];
    var sortedNews = [];
    var feedTitle;
    
    $.headerTitle.text = _responseData && _responseData.feed && _responseData.feed.title;
    
    sortedNews = _.sortBy(newsData.items, function(_item) { 
        return _item.pubDate; 
    });
    
    sortedNews.reverse();
    
    _.each(sortedNews, function(_sortedNew) {
        var newsListItem = Alloy.createController('newsListItem', {
            newsInfo: _sortedNew
        });
                
        listItems.push(newsListItem.getListItem());
    });
        
    $.newsSection.items = listItems;
    
    $.newsListView.sections = [$.newsSection];
}

/**
 * @method handleNewsListViewItemClick
 * @private
 * Opens a new window with the pressed list item
 * @param {Object} _evt Default event object
 * @return {void}
 */
function handleNewsListViewItemClick(_evt) {
    var itemIndex = _evt.itemIndex;
    var itemSelected = $.newsSection.items[itemIndex];
    var url = itemSelected.link.text;
    var title = itemSelected.title.text;    
    
    navigation.openNewsDetailWindow({
        title: title,
        url: url
    });
    
}

$.newsListView.addEventListener('itemclick', handleNewsListViewItemClick);

init();
