var args = $.args;
var navigation = require('navigation');

/**
 * @method init
 * @private
 * Initializes controller
 * @return {void}
 */
function init() {
    var title = args.title;
    var urlToLoad = args.url;
    $.webview.url = urlToLoad;
    
    $.headerTitle.text = title;
}

/**
 * @method handleCloseImageClick
 * @private
 * Handles click event on close image 
 * @param {Object} _evt Event object
 * @return {void}
 */
function handleCloseImageClick(_evt) {
    navigation.closeNewsDetailWindow();
}

$.closeImage.addEventListener('click', handleCloseImageClick);

if (OS_ANDROID) {
    $.window.addEventListener('androidback', handleCloseImageClick);
}

init();
