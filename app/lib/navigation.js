var navigation = (function () {
    
    // All controllers should be defined here
    var controller = {
        indexWindow: null,
        newsDetailWindow: null
    };
    
    /**
     * @method openIndexWindow
     * @public
     * Open index window
     * @param {Ti.UI.Window} _window index window to open
     * @return {void}
     */
    function openIndexWindow(_window) {
                
        if(!controller.indexWindow) {
            controller.indexWindow = _window;
            controller.indexWindow && controller.indexWindow.open();
        }
        
    }
    
    /**
     * @method closeIndexWindow
     * @public
     * Closes index
     * @return {void}
     */
    function closeIndexWindow(){
        if (controller.indexWindow) {
            controller.indexWindow.close();
            controller.indexWindow = null;
        }
    }
    
    /**
     * @method openNewsDetailWindow
     * @public
     * Opens news detail window
     * @param {Object} _params Params to pass for the controller
     * @return {void}
     */
    function openNewsDetailWindow(_params) {
        if(!controller.newsDetailWindow) {
            controller.newsDetailWindow = Alloy.createController('newsDetailWindow', _params);
            controller.newsDetailWindow.window && controller.newsDetailWindow.window.open({
                modal: true
            });
        }
    }
    
    /**
     * @method closeNewsDetailWindow
     * @public
     * Closes news detail window
     * @return {void}
     */
    function closeNewsDetailWindow(){
        if (controller.newsDetailWindow) {
            controller.newsDetailWindow.window && controller.newsDetailWindow.window.close();
            controller.newsDetailWindow = null;
        }
    }
    
    return {
        openIndexWindow: openIndexWindow,
        closeIndexWindow: closeIndexWindow,
        openNewsDetailWindow: openNewsDetailWindow,
        closeNewsDetailWindow: closeNewsDetailWindow
    }
    
})();

module.exports = navigation;