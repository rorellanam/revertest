var webservices = (function () {
    
    /**
     * @method hasConnection
     * @private
     * Determines if user is online or not
     * @return {Boolean}
     */
    function hasConnection() {
        return Ti.Network.online;
    }
    
    /**
     * @method httpRequest
     * @private
     * Defines a XHTTP client to make http calls
     * @param {Object} _params Holds properties to config simple http request 
     * @return {void}
     */
    function httpRequest (_params) {
        var url = _params.url;
        var type = _params.type;
        var timeout = _params.timeout;
        var successCallback = _params.successCallback;
        var failCallback = _params.failCallback;
                
        if (hasConnection()) {
            var httpClient = Ti.Network.createHTTPClient({
                timeout: timeout
            });
            
            httpClient.onload = function (_e) {
                var responseText = JSON.parse(this.responseText);
                successCallback && successCallback(responseText);
            }
                
            httpClient.onerror = function(_e) {
                failCallback && failCallback(_e.error);
            }
            
            httpClient.open(type, url);
            httpClient.send();
        } else {
            failCallback && failCallback({
                status: 'connectionError'
            });
        }
        
    }
    
    /**
     * @method fetchNews
     * @public
     * Fetches news information
     * @param {Object} _params Holds properties to config request 
     * @return {void}
     */
    function fetchNews(_params) {
        var params = _params || {};
        var type = 'GET';
        var timeout = 4000;
        var baseUrl = Alloy.Globals.baseUrl;
        var successCallback = _params.successCallback;
        var failCallback = _params.failCallback;
        
        httpRequest({
            type: type,
            url: baseUrl,
            timeout: timeout,
            successCallback: successCallback,
            failCallback: failCallback
        });
        
    }
    
    return {
        fetchNews: fetchNews
    }
    
})();

module.exports = webservices;