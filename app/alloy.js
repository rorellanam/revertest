var moment = require('alloy/moment');

(function() {
    
    
    Alloy.Globals = {
        // urls
        baseUrl: 'http://rss2json.com/api.json?rss_url=https%3A%2F%2Fnews.ycombinator.com%2Frss',
        
        
        //global colors that might be used across the app
        colors: {
            supernova: '#FFD008',
            brightTurquoise: '#14CFDD',
            gray: '#808080',
            doveGray: '#636363',
            white: '#FFFFFF',
            lightningYellow: '#FBC02D',
            alabaster: '#FAFAFA',
            transparent: 'transparent'
        }  
    };
    
    

})();